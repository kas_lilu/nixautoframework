package nix.training.intensive.spec

import geb.spock.GebReportingSpec
import nix.training.intensive.page.BlogPage
import nix.training.intensive.page.StartPage

class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to Blog page"() {
        when:
            to StartPage
        and:
            "User navigates to Blog page"()
        then:
            at BlogPage
    }
}
